from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
# from selenium.webdriver.firefox.service import Service as FirefoxService
# from webdriver_manager.firefox import GeckoDriverManager
# from selenium.webdriver.edge.service import Service as EdgeService
# from webdriver_manager.microsoft import EdgeChromiumDriverManager
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

# driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
# driverFireFox = webdriver.Firefox(service=FirefoxService(GeckoDriverManager().install()))
# driverEdge = webdriver.Edge(service=EdgeService(EdgeChromiumDriverManager().install()))
# driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))





"""
*Testcase Web:
1. Open google.com > input "python" in search > return over 3 result
2. Open google.com > input "python" in search > click the 2nd result
"""


def test_TC01_Show3TopResult():
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    
    #search text 'python'
    driver.get("http://www.google.com/")
    driver.maximize_window()
    search_field_xpath = ("//textarea[@type='search']")
    text_search = "Python"
    search_field = driver.find_element(By.XPATH,search_field_xpath)
    search_field.send_keys(text_search)
    search_field.send_keys(Keys.ENTER)

    #check search result > 3
    searchResult_list_xpath = ("//a//h3")
    listResult = driver.find_elements(By.XPATH,searchResult_list_xpath)
    time.sleep(2)
    driver.quit()
    assert len(listResult) >= 3
    


def test_TC02_click1stResult():
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    
    #search text 'python'
    driver.get("http://www.google.com/")
    driver.maximize_window()
    search_field_xpath = ("//textarea[@type='search']")
    text_search = "Python"
    search_field = driver.find_element(By.XPATH,search_field_xpath)
    search_field.send_keys(text_search)
    search_field.send_keys(Keys.ENTER)

    #click 2nd place in search result, then check title
    searchResult_2ndplace_xpath = ("(//a//h3)[2]")    
    searchResult_2ndPlace = driver.find_element(By.XPATH,searchResult_2ndplace_xpath)
    searchResult_2ndPlace.click()
    title = driver.title
    expectedTitle = "Python (ngôn ngữ lập trình)"    
    time.sleep(2)
    driver.quit()
    assert expectedTitle in title

if __name__ == "__main__":
    test_TC01_Show3TopResult()
    test_TC02_click1stResult()
      





